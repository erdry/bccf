<?php if ( is_active_sidebar( 'sideright' ) ) : ?>
<div class="col-md-5 sidebar">
    <aside class="widget-area">
        <?php dynamic_sidebar( 'sideright' ); ?>
    </aside>
</div>
<?php endif; ?>