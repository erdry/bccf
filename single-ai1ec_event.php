<?php get_header(); ?>
   <div class="container wrapper">
    <div class="row">
        <div class="col-md-9">
            <?php while ( have_posts() ) : the_post(); ?>

            <?php get_template_part( 'content', 'page' ); ?>

            <?php endwhile; ?>

        </div>
        <?php get_sidebar(); ?>
    </div>
</div>
<?php get_footer(); ?>