<?php
/**
 * Template Name: Blog
 *
 */

get_header(); ?>

	<div class="whats-on-rumata">
			<div class="container wrapper">
				<div class="row">
					<div class="col-md-8">
						<section class="content">
							<article class="post">
								<header class="entry-header">
									<div class="entry-cover">
										<a href="whats-on-rumata-post.html">
											<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/slide-02.jpg" alt="">
										</a>
									</div>
									<h2 class="entry-title"><a href="whats-on-rumata-post.html">(Press Release) Acara Persembahan Yayasan Kalla Menandai Dibukanya Secara Resmi Makassar SEAScreen Academy 2013</a></h2>
									<div class="entry-meta">
										<span class="meta-category"><a href="">Uncategorized</a></span> <span class="meta-date">October 5, 2013</span> <span class="meta-comments"><a href="">1 Comments</a></span>
									</div>
								</header>
								<div class="entry-content">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis, veritatis provident quia delectus praesentium inventore! Molestiae, ea consectetur voluptatum maiores sint accusantium neque voluptatibus quas! Aspernatur esse porro provident alias!</p>
									<a class="readmore" href="whats-on-rumata-post.html">Readmore</a>
								</div>
							</article>

							<article class="post">
								<header class="entry-header">
									<div class="entry-cover">
										<a href="whats-on-rumata-post.html">
											<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/slide-01.jpg" alt="">
										</a>
									</div>
									<h2 class="entry-title"><a href="whats-on-rumata-post.html">Bekerja dengan Kru dan Memilih Lokasi - Sisi Ketiga dan Keempat</a></h2>
									<div class="entry-meta">
										<span class="meta-category"><a href="">Press</a></span> <span class="meta-date">October 5, 2013</span> <span class="meta-comments"><a href="">1 Comments</a></span>
									</div>
								</header>
								<div class="entry-content">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, quidem temporibus commodi voluptates culpa quae consequatur dolores omnis! Ad, nihil assumenda distinctio tenetur ullam vero nobis vitae minima rerum voluptatibus.</p>
									<a class="readmore" href="whats-on-rumata-post.html">Readmore</a>
								</div>
							</article>

							<article class="post">
								<header class="entry-header">
									<div class="entry-cover">
										<a href="whats-on-rumata-post.html">
											<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/slide-02.jpg" alt="">
										</a>
									</div>
									<h2 class="entry-title"><a href="whats-on-rumata-post.html">Pimpaka Towira: "There are no limits to what you can do in filmmaking"</a></h2>
									<div class="entry-meta">
										<span class="meta-category"><a href="">Film</a></span> <span class="meta-date">October 5, 2013</span> <span class="meta-comments"><a href="">1 Comments</a></span>
									</div>
								</header>
								<div class="entry-content">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis, veritatis provident quia delectus praesentium inventore! Molestiae, ea consectetur voluptatum maiores sint accusantium neque voluptatibus quas! Aspernatur esse porro provident alias!</p>
									<a class="readmore" href="whats-on-rumata-post.html">Readmore</a>
								</div>
							</article>

							<ul class="pagination">
								<li><a href="#">&laquo;</a></li>
								<li class="active"><a href="#">1</a></li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#">4</a></li>
								<li><a href="#">5</a></li>
								<li><a href="#">&raquo;</a></li>
							</ul>
						</section>
					</div>
					<div class="col-md-4">
						<aside class="widget-area">
							<div class="widget">
								<div class="programme-banner">
									<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/programme-02.jpg" alt="">
									<a class="caption-banner" href="programmes.html">
										<h6 class="caption-text">Sea Screen</h6>
									</a>
								</div>
								<br>
								<br>
								<div class="programme-banner">
									<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/programme-01.jpg" alt="">
									<a class="caption-banner" href="programmes.html">
										<h6 class="caption-text">Finding Sincerity</h6>
									</a>
								</div>
							</div><!-- ./widget-->

							<div class="widget widget-categories">
								<h3 class="widget-title">Categories</h3>
								<ul>
									<li><a href="">Event</a></li>
									<li><a href="">Press</a></li>
									<li><a href="">Interview</a></li>
									<li><a href="">Schedule</a></li>
									<li><a href="">Principal</a></li>
									<li><a href="">Lecturers</a></li>
									<li><a href="">Participants</a></li>
									<li><a href="">Journal</a></li>
								</ul>
							</div>

							<div class="widget">
								<h3 class="widget-title">Latest Tweets</h3>
								<a class="twitter-timeline" href="https://twitter.com/dehamzah" data-widget-id="521313525705101312">Tweets by @dehamzah</a>
								<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
							</div>

							<div class="widget">
								<img src="http://placehold.it/300x300&text=Facebook Like Box" alt="">
							</div>
						</aside>
					</div>
				</div>
			</div><!-- /.container-->
		</div>
		
	
<?php get_footer(); ?>