<?php
/**
 * bccf functions and definitions.
 *
 * Sets up the theme and provides some helper functions, which are used
 * in the theme as custom template tags. Others are attached to action and
 * filter hooks in WordPress to change core functionality.
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are instead attached
 * to a filter or action hook.
 *
 * For more information on hooks, actions, and filters, see http://codex.wordpress.org/Plugin_API.
 *
 */


/**
 * Sets up theme defaults and registers the various WordPress features that
 * the bccf theme supports.
 *
 * @uses add_editor_style() To add a Visual Editor stylesheet.
 * @uses add_theme_support() To add support for post thumbnails
 * @uses register_nav_menu() To add support for navigation menus.
 * @uses set_post_thumbnail_size() To set a custom post thumbnail size.
 *
 */

// change submenu class 
require_once('wp_bootstrap_navwalker.php');

function bccf_setup() {

	// This theme styles the visual editor with editor-style.css to give it some niceties.
	add_editor_style();
	// This theme uses wp_nav_menu() in one location.
	register_nav_menu( 'primary', __( 'Top Menu', 'bccf' ) );
	register_nav_menu( 'secondary', __( 'Bottom Menu', 'bccf' ) );

	// This theme uses a custom image size for featured images, displayed on "standard" posts.
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 637, 9999 ); // Unlimited height, soft crop
    if ( function_exists( 'add_image_size' ) ) {
        add_image_size( 'covermini', 293, 180, true ); //(cropped)
    }
}
add_action( 'after_setup_theme', 'bccf_setup' );


/**
 * Enqueues scripts and styles for front-end.
 */
function bccf_scripts_styles() {
	global $wp_styles;

	/*
	 * Loads our main stylesheet.
	 */
	wp_enqueue_style( 'bccf-style', get_stylesheet_uri() );

	/*
	 * Optional: Loads the Internet Explorer specific stylesheet.
	 */
	//wp_enqueue_style( 'bccf-ie', get_template_directory_uri() . '/css/ie.css', array( 'bccf-style' ), '20121010' );
	//$wp_styles->add_data( 'bccf-ie', 'conditional', 'lt IE 9' );
}
add_action( 'wp_enqueue_scripts', 'bccf_scripts_styles' );
// widgets area
function bccf_sidebars() {
	register_sidebar(array(
    'name' => __( 'Sidebar Right' ),
    'id' => 'sideright',
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
    ));
}
add_action( 'widgets_init', 'bccf_sidebars' );
// custom excerpt
function custom_excerpt_length( $length ) {
	return 20;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
function new_excerpt_more( $more ) {
	return '';
}
add_filter('excerpt_more', 'new_excerpt_more');
// title
function custom_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() ) {
		return $title;
	}

	// Add the site name.
	$title .= get_bloginfo( 'name', 'display' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) ) {
		$title = "$title $sep $site_description";
	}

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 ) {
		$title = "$title $sep " . sprintf( __( 'Page %s', 'bccf' ), max( $paged, $page ) );
	}

	return $title;
}
add_filter( 'wp_title', 'custom_wp_title', 10, 2 );
// slider post type
function slider_post_type() {

	$labels = array(
		'name'                => _x( 'Sliders', 'Post Type General Name', 'bccf' ),
		'singular_name'       => _x( 'Slider', 'Post Type Singular Name', 'bccf' ),
		'menu_name'           => __( 'Sliders', 'bccf' ),
		'parent_item_colon'   => __( 'Parent Slider:', 'bccf' ),
		'all_items'           => __( 'All Sliders', 'bccf' ),
		'view_item'           => __( 'View Slider', 'bccf' ),
		'add_new_item'        => __( 'Add New Slider', 'bccf' ),
		'add_new'             => __( 'Add New', 'bccf' ),
		'edit_item'           => __( 'Edit Slider', 'bccf' ),
		'update_item'         => __( 'Update Slider', 'bccf' ),
		'search_items'        => __( 'Search Slider', 'bccf' ),
		'not_found'           => __( 'Not found', 'bccf' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'bccf' ),
	);
	$args = array(
		'label'               => __( 'slider', 'bccf' ),
		'description'         => __( 'Manage Sliders', 'bccf' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 20,
		'menu_icon'           => 'dashicons-images-alt2',
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => true,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);
	register_post_type( 'slider', $args );

}
add_action( 'init', 'slider_post_type', 0 );


// my function
// Register a new shortcode: [cr_custom_registration]
add_shortcode( 'cr_custom_registration', 'custom_registration_shortcode' );
 
// The callback function that will replace [book]
function custom_registration_shortcode() {
    ob_start();
    custom_registration_function();
    return ob_get_clean();
}

// end of myfunction