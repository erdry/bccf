<?php get_header(); ?>
<div class="container wrapper">
    <div class="row">
        <div class="col-md-12 content">
            <?php while ( have_posts() ) : the_post(); ?>

            <?php get_template_part( 'content', 'page' ); ?>

            <?php endwhile; ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>